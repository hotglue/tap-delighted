from typing import Any, Dict, Optional

import requests
from singer_sdk import typing as th

from tap_delighted.client import DelightedStream
import urllib.parse


class PeopleStream(DelightedStream):
    """Define custom stream."""

    name = "people"
    path = "/people.json"
    primary_keys = ["id"]
    replication_key = "created_at"
    per_page = 100

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("last_sent_at", th.DateTimeType),
        th.Property("last_responded_at", th.DateTimeType),
        th.Property("next_survey_scheduled_at", th.DateTimeType),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        next_page_token = response.headers.get("Link", None)
        if next_page_token:
            next_page_token = next_page_token.split("&")[0][1:]
            next_page_token = next_page_token.split("page_info=")[-1]
            next_page_token = urllib.parse.unquote(next_page_token)
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:

        params: dict = {}

        params["per_page"] = self.per_page

        start_date = self.get_starting_timestamp(context)
        if self.replication_key:
            start_date = round(start_date.timestamp()) + 1
            params["since"] = start_date

        if next_page_token:
            params["page_info"] = next_page_token
        return params


class SurveyResponsesStream(DelightedStream):
    """Define Responses stream."""

    name = "survey_responses"
    path = "/survey_responses.json"
    primary_keys = ["id"]
    replication_key = "updated_at"
    per_page = 100

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("person", th.StringType),
        th.Property("survey_type", th.StringType),
        th.Property("score", th.IntegerType),
        th.Property("comment", th.StringType),
        th.Property("permalink", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property(
            "person_properties",
            th.PropertiesList(
                th.Property("Delighted Source", th.StringType),
                th.Property("Delighted Device Type", th.StringType),
                th.Property("Delighted Operating System", th.StringType),
                th.Property("Delighted Browser", th.StringType),
            ),
        ),
        th.Property("notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("tags", th.CustomType({"type": ["array", "string"]})),
        th.Property("additional_answers", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:

        if not previous_token:
            previous_token = 1

        if response.json():
            next_page_token = previous_token + 1
            return next_page_token
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:

        params: dict = {}

        params["per_page"] = self.per_page

        start_date = self.get_starting_timestamp(context)
        if self.replication_key:
            start_date = round(start_date.timestamp()) + 1
            params["updated_since"] = start_date
            params["order"] = "asc:updated_at"

        if next_page_token:

            params["page"] = next_page_token
        return params


class MetricsStream(DelightedStream):
    name = "metrics"
    path = "/metrics.json"

    schema = th.PropertiesList(
        th.Property("nps", th.IntegerType),
        th.Property("promoter_count", th.IntegerType),
        th.Property("promoter_percent", th.NumberType),
        th.Property("passive_count", th.IntegerType),
        th.Property("passive_percent", th.NumberType),
        th.Property("detractor_count", th.IntegerType),
        th.Property("detractor_percent", th.NumberType),
        th.Property("happiness", th.IntegerType),
        th.Property("very_happy_count", th.IntegerType),
        th.Property("very_happy_percent", th.NumberType),
        th.Property("happy_count", th.IntegerType),
        th.Property("happy_percent", th.NumberType),
        th.Property("neutral_count", th.IntegerType),
        th.Property("neutral_percent", th.NumberType),
        th.Property("unhappy_count", th.IntegerType),
        th.Property("unhappy_percent", th.NumberType),
        th.Property("very_unhappy_count", th.IntegerType),
        th.Property("very_unhappy_percent", th.NumberType),
        th.Property("response_count", th.IntegerType),
    ).to_dict()
