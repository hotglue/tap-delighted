from datetime import datetime
from typing import Optional

from backports.cached_property import cached_property
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.streams import RESTStream


class DelightedStream(RESTStream):

    url_base = "https://api.delighted.com/v1"

    records_jsonpath = "$[*]"

    @property
    def authenticator(self) -> BasicAuthenticator:
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("api_key"),
            password="",
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    @cached_property
    def datetime_fields(self):
        datetime_fields = []
        for key, value in self.schema["properties"].items():
            if value.get("format") == "date-time":
                datetime_fields.append(key)
        return datetime_fields

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        for field in self.datetime_fields:
            if row.get(field):
                dt_field = datetime.utcfromtimestamp(int(row[field]))
                row[field] = dt_field.isoformat()
        return row
